/*eslint-disable */
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');
const cache = require('gulp-cache');
const pug = require('gulp-pug');
const notify = require('gulp-notify');


gulp.task('sass', function () {
  return gulp.src('./src/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'expanded', errLogToConsole: true }).on("error", notify.onError("Error: <%= error.message %>")))
    .pipe(postcss([
      require('postcss-inline-svg'),
      require('postcss-svgo'),
      autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
      }),
    ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream({match: "**/*.css"}));
});


gulp.task('html', function () {
  return gulp.src('./src/*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('components', function () {
  return gulp.src('./src/**/*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('image', function () {
  return gulp.src('./src/assets/images/**/*.+(png|jpg|gif|svg)')
    //.pipe(cache(imagemin()))
    .pipe(gulp.dest('./dist/assets/images'));
});

gulp.task('js', function () {
  return gulp.src('./src/js/**/*')
    .pipe(gulp.dest('./dist/js'))
    .pipe(browserSync.stream());
});

gulp.task('fonts', function() {
  return gulp.src('./src/assets/fonts/**/*')
    .pipe(gulp.dest('./dist/assets/fonts'))
})

gulp.task('serve', ['sass'], function () {
  browserSync.watch(['./src/main.scss', 'src/scss/**/*.scss', 'src/components/**/*.scss'], function () {
    gulp.start('sass');
  });

  browserSync.watch(['./src/*.pug', 'src/components/**/*.pug'], function () {
    gulp.start('html');
  });

  browserSync.watch('./src/js/*.js', function () {
    gulp.start('js');
  });

  browserSync.watch('./src/assets/images/**/*', function () {
    gulp.start('image');
  });

  browserSync.watch('./src/assets/fonts/**/*', function () {
    gulp.start('fonts');
  });

  browserSync.watch('./dist/assets/images/**/*', function () {
    browserSync.reload();
  });

  browserSync.watch('./dist/*.html').on('change',  browserSync.reload);

  browserSync.init({
    server: './dist'
  });
});


gulp.task('sort-css', function () {
  const sorting = require('postcss-sorting');

  return gulp
    .src('./dist/css/*.css').pipe(
    postcss([
      sorting({ 'clean-empty-lines': true })
    ])
  ).pipe(
    gulp.dest('./dist/css')
  );
});


gulp.task('pug', function () {
  return gulp.src('./src/components/**/*.pug', { base: "." })
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest('./'));
});


gulp.task('dev', ['sass', 'html', 'image', 'js', 'fonts', 'serve']);
gulp.task('default', ['dev']);
/*eslint-enable */
